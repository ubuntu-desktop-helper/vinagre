<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="keyboard-shortcuts" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index#options"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Desabilite o envio de atalhos de teclado para a máquina remota.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013.</mal:years>
    </mal:credit>
  </info>

  <title>Atalhos de teclado</title>

  <p>Você pode habilitar ou desabilitar atalhos de teclado ao selecionar a opção <guiseq><gui style="menu">Ver</gui> <gui style="menuitem">Atalhos de teclado</gui></guiseq>.</p>

  <p>Ao habilitar esta opção, permite-se o uso de atalhos de teclado (como <keyseq><key>Ctrl</key><key>N</key></keyseq>), aceleradores de menu (como <keyseq><key>Alt</key><key>M</key></keyseq>) e mnemônicos, com o aplicativo <app>Visualizador de área de trabalho remota</app>. Isto significa que o aplicativo <app>Visualizador de área de trabalho remota</app> interceptará estas combinações de teclas, ao invés de enviá-las para a máquina remota. Por padrão, esta opção é desabilitada porque na maior parte do tempo, você estará interagindo com o computador o qual está conectado.</p>

  <note>
    <p>Quando a opção <gui>Atalhos de teclado</gui> está desabilitada, <keyseq><key>Ctrl</key><key>Alt</key><key>Del</key></keyseq> é a única combinação que não será enviada à área de trabalho remota. Selecione a opção <guiseq><gui style="menu">Remoto</gui><gui style="menuitem">Enviar Ctrl-Alt-Del</gui></guiseq>, ou pressione o botão da barra de ferramentas correspondente, para enviar esta combinação de tecla.</p>
  </note>

</page>
