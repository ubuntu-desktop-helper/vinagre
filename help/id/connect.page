<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="id">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Menyambung ke komputer lain pada jaringan lokal Anda.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Andika Triwidada</mal:name>
      <mal:email>andika@gmail.com</mal:email>
      <mal:years>2013, 2014, 2016.</mal:years>
    </mal:credit>
  </info>

  <title>Menjalin koneksi</title>

  <p>Anda dapat menyambung ke komputer lain pada jaringan lokal Anda memakai <app>Penampil Desktop Jauh</app>.</p>

  <steps>
    <item>
      <p>Pilih <guiseq><gui style="menu">Jarak Jauh</gui> <gui style="menuitem">Sambung</gui></guiseq>.</p>
    </item>
    <item>
      <p>Pilih <gui>Protokol</gui> dan <gui>Host</gui> untuk koneksi.</p>
      <note>
        <p>Beberapa protokol memungkinkan Anda melihat semua komputer yang tersedia pada jaringan lokal Anda dengan mengklik tombol <gui style="button">Cari</gui>. Tombol ini tak akan ditampilkan bila Anda tak memiliki dukungan <app>Avahi</app>.</p>
      </note>
    </item>
    <item>
      <p>Pilih opsi yang ingin Anda fungsikan ketika koneksi dibuat. Opsi akan bervariasi bergantung kepada protokol yang Anda pakai.</p>
    </item>
    <item>
      <p>Klik tombol <gui style="button">Sambung</gui>.</p>
      <p>Pada titik ini, desktop jarak jauh mungkin perlu mengkonfirmasi koneksi. Bila ini yang terjadi, penilik mungkin tetap hitam sampai koneksi dikonfirmasi.</p>
      <note>
        <p>Beberapa komputer mungkin memerlukan koneksi aman: dialog otentikasi akan ditampilkan, meminta kredensial. Tipe kredensial bergantung kepada host remote; mungkin berupa nama pengguna dan sandi. Bila Anda memilih <gui>Ingat kredensial ini</gui>, <app>Penampil Desktop Jauh</app> akan menyimpan informasi itu memakai <app>GNOME Ring Kunci</app>.</p>
      </note>
    </item>
  </steps>

  <p>Bila koneksi pernah dipakai sebelumnya, Anda juga dapat mengaksesnya melalui <guiseq><gui style="menu">Remote</gui> <gui style="menuitem">Koneksi Baru-baru Ini</gui></guiseq>.</p>

  <p>Untuk menutup suatu koneksi, pilih <guiseq><gui style="menu">Remote</gui> <gui style="menuitem">Tutup</gui></guiseq> atau klik tombol <gui style="button">Tutup</gui>.</p>

</page>
