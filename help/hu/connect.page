<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="hu">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Kapcsolódás másik számítógéphez a helyi hálózaton.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Kelemen Gábor</mal:name>
      <mal:email>kelemeng at gnome dot hu</mal:email>
      <mal:years>2011, 2012, 2013, 2014.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>n0m4dm4n</mal:name>
      <mal:email>n0m4dm4n at gmail dot com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

  <title>Kapcsolat létrehozása</title>

  <p>A <app>Távoli asztalok megjelenítése</app> segítségével csatlakozhat a helyi hálózat más számítógépeihez.</p>

  <steps>
    <item>
      <p>Válassza a <guiseq><gui style="menu">Távoli</gui> <gui style="menuitem">Kapcsolódás</gui></guiseq> menüpontot.</p>
    </item>
    <item>
      <p>Válasszon a <gui>Protokoll</gui> listából, és töltse ki a <gui>Gép</gui> mezőt a kapcsolódáshoz.</p>
      <note>
        <p>Néhány protokoll lehetővé teszi a helyi hálózaton elérhető összes számítógép megjelenítését a <gui style="button">Keresés</gui> gombra kattintva. Az <app>Avahi</app> támogatás nélkül ez a gomb nem jelenik meg.</p>
      </note>
    </item>
    <item>
      <p>Válasszon a kapcsolat létrehozásához használandó beállítások közül, ezek a használt protokoll függvényében változnak.</p>
    </item>
    <item>
      <p>Nyomja meg a <gui style="button">Kapcsolódás</gui> gombot.</p>
      <p>Ezen a ponton előfordulhat, hogy a távoli asztalnak jóvá kell hagynia a kapcsolódást. Ebben az esetben a megjelenítő fekete maradhat, amíg a jóváhagyása nem érkezik meg.</p>
      <note>
        <p>Bizonyos kapcsolatoknál hitelesítés szükséges, ekkor megjelenik egy párbeszédablak. Itt meg kell adnia a belépéshez szükséges adatokat, ezek a távoli géptől függően például felhasználónév és jelszó lehetnek. Ha bejelöli az <gui>Emlékezzen a hitelesítési adatokra</gui> négyzetet, akkor a <app>Távoli asztalok megjelenítése</app> elmenti azt a kulcstartójára.</p>
      </note>
    </item>
  </steps>

  <p>A régebben használt kapcsolatok elérhetők a <guiseq><gui style="menu">Távoli</gui> <gui style="menuitem">Legutóbbi kapcsolatok</gui></guiseq> menüpontban.</p>

  <p>A kapcsolat bezárásához válassza a <guiseq><gui style="menu">Távoli</gui> <gui style="menuitem">Bezárás</gui></guiseq> menüpontot, vagy nyomja meg a <gui style="button">Bezárás</gui> gombot.</p>

</page>
