<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="scaling" xml:lang="de">

  <info>
    <link type="guide" xref="index#options"/>

    <revision version="0.2" pkgversion="3.8" date="2013-03-23" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@googlemail.com</email>
    </credit>

    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

    <desc>Entfernten Bildschirm so skalieren, dass er in den Anzeigebereich passt.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2008, 2009, 2011, 2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>Christian.Kirbach@googlemail.com</mal:email>
      <mal:years>2009,2014,2016.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamin Steinwender</mal:name>
      <mal:email>b@stbe.at</mal:email>
      <mal:years>2014.</mal:years>
    </mal:credit>
  </info>

  <title>Entfernten Bildschirm skalieren</title>

  <p>Einige Protokolle unterstützen die Einstellung, den entfernten Bildschirm so zu skalieren, dass er in Ihren Anzeigebereich passt.</p>

  <steps>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Ansicht</gui><gui style="menuitem">Skalieren</gui></guiseq> oder klicken Sie auf den <gui>Skalieren</gui>-Knopf in der Werkzeugleiste.</p>
    </item>
    <item>
      <p>Wählen Sie <guiseq><gui style="menu">Ansicht</gui><gui style="menuitem">Seitenverhältnis beibehalten</gui></guiseq>, wenn Sie das Seitenverhältnis des entfernten Bildschirms innerhalb des Anzeigebereichs beibehalten möchten, oder wählen Sie diese Option ab, um den entfernten Bildschirm zu strecken, um den verfügbaren Bereich zu füllen.</p>
      <note>
        <p>Diese Einstellung kann nur geändert werden, wenn <gui>Skalierung</gui> bereits aktiviert ist.</p>
      </note>
    </item>
  </steps>

</page>
