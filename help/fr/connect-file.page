<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="guide" id="connect-file" xml:lang="fr">

  <info>
    <link type="guide" xref="index#connections"/>
    <link type="seealso" xref="connect"/>
    <title type="sort">2</title>

    <revision version="0.2" pkgversion="3.8" date="2013-03-23" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email>kittykat3756@googlemail.com</email>
    </credit>

    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>

    <desc>Utiliser un fichier de connexion distant pour se connecter à une machine distante.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Julien Hardelin</mal:name>
      <mal:email>jhardlin@orange.fr</mal:email>
      <mal:years>2011, 2013.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011.</mal:years>
    </mal:credit>
  </info>

  <title>Connexion avec un fichier <file>.vnc</file></title>

  <p>Certains hôtes fournissent des <em>fichiers de connexion distants</em> (généralement terminés par le suffixe <file>.vnc</file>), au lieu de recourir à une adresse et à un port d'hôte.</p>

  <steps>
    <item>
      <p>Choisissez <guiseq><gui style="menu">Distant</gui> <gui style="menuitem">Ouvrir</gui></guiseq>.</p>
    </item>
    <item>
      <p>Sélectionnez le fichier à ouvrir.</p>
    </item>
    <item>
      <p>Cliquez sur le bouton <gui style="button">Ouvrir</gui>.</p>
    </item>
  </steps>

  <note style="tip">
    <p>Vous pouvez aussi ouvrir un fichier de connexion distante à partir de <app>Fichiers</app>.</p>
  </note>

</page>
