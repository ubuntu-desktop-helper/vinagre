<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="connect" xml:lang="cs">

  <info>
    <link type="guide" xref="index#connections"/>
    <title type="sort">1</title>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Připojte se k jinému počítači ve své místní síti.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  </info>

  <title>Jak ustavit spojení</title>

  <p>Pomocí <app>Prohlížeče vzdálené plochy</app> se můžete připojit k ostatním počítačům ve své místní síti.</p>

  <steps>
    <item>
      <p>Vyberte <guiseq><gui style="menu">Vzdálené</gui> <gui style="menuitem">Připojit</gui></guiseq>.</p>
    </item>
    <item>
      <p>Vyberte <gui>Protokol</gui> a <gui>Počítač</gui> pro připojení.</p>
      <note>
        <p>Některé protokoly vám umožní po kliknutí na tlačítko <gui style="button">Hledat</gui>, vidět všechny dostupné počítače ve vaší místní síti. Toto tlačítko se ale nezobrazí, pokud nemáte nainstalovánu podporu <app>Avahi</app>.</p>
      </note>
    </item>
    <item>
      <p>Vyberte volby, které chcete použít při vytváření spojení. Volby se liší v závislosti na použitém protokolu.</p>
    </item>
    <item>
      <p>Klikněte na tlačítko <gui style="button">Připojit</gui>.</p>
      <p>V tuto chvíli může vzdálená plocha požadovat potvrzení připojení. V takovém případě může prohlížeč zobrazovat černou plochu tak dlouho, dokud není připojení schváleno.</p>
      <note>
        <p>Některé počítače mohou mít zabezpečení připojení: zobrazí se dialogové okno s dotazem na vaše pověření. Typ pověření závisí na vzdáleném počítači, může to být například jméno a heslo. Když vyberete <gui>Zapamatovat si pověření</gui>, uloží <app>Prohlížeč vzdálené plochy</app> informace do <app>Klíčenky GNOME</app>.</p>
      </note>
    </item>
  </steps>

  <p>Pokud bylo připojení použito již dříve, můžete s k němu dostat také přes <guiseq><gui style="menu">Vzdálené</gui> <gui style="menuitem">Nedávná připojení</gui></guiseq>.</p>

  <p>Pro uzavření spojení zvolte <guiseq><gui style="menu">Vzdálené</gui> <gui style="menuitem">Odpojit</gui></guiseq> nebo klikněte na tlačítko <gui style="button">Odpojit</gui>.</p>

</page>
